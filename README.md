Power Guitar

![powerguitarscreenshot.png](https://s27.postimg.org/lkwa58igz/powerguitarscreenshot.png)

A guitar hero inspired game for the [Gamebuino](gamebuino.com)

How to play:

**LEFT**: pick line 1
**UP**: pick line 2
**DOWN**: pick line 3
**RIGHT**: pick line 4
**A**: hold for "non filled" notes

If you miss 25 notes you lose, highscore will be saved in EEPROM.

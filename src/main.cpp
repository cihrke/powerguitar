#include <SPI.h>
#include <Gamebuino.h>
#include <QueueList.h>
#include <EEPROM.h>

#define RADIUS 2
#define LINE1 11 // left
#define LINE2 23 // up
#define LINE3 35 // down
#define LINE4 47 // right
#define FRAMERATE 40
#define HIGHSCOREADR 0

Gamebuino gb;

// game state
short score = 0;
short oldscore;
short missed = 0;
float speed = 0.1;
bool gameover = false;
bool openNote = false;
bool hitNote[4];
bool levelup = false;
byte lastnoteline = -1;

typedef struct {
    byte line;
    float positionY;
    bool open; // do not or do hold button A
    //TODO: bool hold; // do or do not hold button B
} Note;

// use fixed array for better perfomance!
QueueList<Note> liveNotes;

void println(const char* line, byte x, byte y) {
    gb.display.cursorX = x;
    gb.display.cursorY = y;
    gb.display.println(line);
}

void spawnNote() {
    Note newNote;
    newNote.positionY = 0;
    byte newnoteline;

    while(true) {
        newnoteline = random(0, 4);
        if (newnoteline != lastnoteline) {
            lastnoteline = newnoteline;
            break;
        }
    }

    switch(newnoteline) {
        case 0: newNote.line = LINE1; break;
        case 1: newNote.line = LINE2; break;
        case 2: newNote.line = LINE3; break;
        case 3: newNote.line = LINE4; break;
        default: break;
    }

    if(random(4) == 0) {
        newNote.open = true;
    }
    else {
        newNote.open = false;
    }

    liveNotes.push(newNote);
}

void setup() {
    gb.begin();
    gb.titleScreen(F("Power Guitar"));
    gb.pickRandomSeed();
    gb.battery.show = false;
    gb.setFrameRate(FRAMERATE);

    score = 0;
    missed = 0;
    speed = 0.1;
    gameover = false;
    openNote = false;

    while (!liveNotes.isEmpty()) {
        liveNotes.pop();
    }
}

void loop() {
    if (gb.update() && !gameover) {
        // note generation, currently random TODO tracks with music
        if ((liveNotes.count() == 0 || (random(2) == 0 && gb.frameCount % FRAMERATE == 0))
            && liveNotes.count() < 10) {
            spawnNote();
        }

        // Input
        if (gb.buttons.pressed(BTN_A)) {
            openNote = true;
        }

        if (gb.buttons.released(BTN_A)) {
            openNote = false;
        }

        if (gb.buttons.released(BTN_B)) {
            //spawnNote();
        }

        if (gb.buttons.pressed(BTN_C)) {
            setup();
        }

        if (gb.buttons.pressed(BTN_LEFT)) {
            hitNote[0] = true;
        }

        if (gb.buttons.pressed(BTN_UP)) {
            hitNote[1] = true;
        }

        if (gb.buttons.pressed(BTN_DOWN)) {
            hitNote[2] = true;
        }

        if (gb.buttons.pressed(BTN_RIGHT)) {
            hitNote[3] = true;
        }

        // HUD
        gb.display.setColor(BLACK);
        gb.display.drawFastVLine(60, 0, 46);
        gb.display.drawFastVLine(60, 0, 48);
        println("Score", 62, 3);
        println(String(score).c_str(), 62, 10);
        println("Miss", 62, 17);
        println(String(missed).c_str(), 62, 24);

        // Game area
        gb.display.drawFastVLine(LINE1, 0, LCDHEIGHT);
        gb.display.drawFastVLine(LINE2, 0, LCDHEIGHT);
        gb.display.drawFastVLine(LINE3, 0, LCDHEIGHT);
        gb.display.drawFastVLine(LINE4, 0, LCDHEIGHT);
        gb.display.drawFastHLine(0, 41, 60);
        gb.display.drawFastHLine(0, 44, 60);

        // draw live Notes
        Note tmp;
        short length = liveNotes.count();
        for (short i=0; i<length;i++) {
            tmp = liveNotes.pop();
            // hit detection
            if (hitNote[(tmp.line+1)/12-1] == true && tmp.positionY >= 41
                && tmp.positionY <= 44 && !tmp.open && !openNote) {
                gb.sound.playOK();
                score ++;
                continue;
            }
            // hit detection open note
            if (hitNote[(tmp.line+1)/12-1] == true && tmp.positionY >= 41
                && tmp.positionY <= 44 && tmp.open && openNote) {
                gb.sound.playOK();
                score += 2;
                continue;
            }
            if (tmp.positionY < LCDHEIGHT) {
                if (tmp.open) {
                    gb.display.drawCircle(tmp.line, tmp.positionY, RADIUS);
                }
                else {
                    gb.display.fillCircle(tmp.line, tmp.positionY, RADIUS);
                }
                tmp.positionY += speed;
                liveNotes.push(tmp);
            }
            else if (tmp.positionY >= LCDHEIGHT) {
                missed ++;
                gb.sound.playCancel();
            }
        }

        if (missed >= 25) {
            gameover = true;
        }
        if (score % 20 == 0) {
            score ++;
            speed += 0.1;
        }

        hitNote[0] = false;
        hitNote[1] = false;
        hitNote[2] = false;
        hitNote[3] = false;
    }
    else if (gameover) {
        gb.display.clear();
        println("Game over!", 20, 15);
        println("Final score", 20, 22);
        println(String(score).c_str(), 20, 29);

        // save highscore
        EEPROM.get(HIGHSCOREADR, oldscore);
        if (oldscore < score) {
            gb.popup(F("New Highscore!"), FRAMERATE*5);
            EEPROM.put(HIGHSCOREADR, score);
        }

        if (gb.buttons.pressed(BTN_C)) {
            setup();
        }
    }
}
